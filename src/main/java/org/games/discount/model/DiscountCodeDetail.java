package org.games.discount.model;

import org.games.discount.enums.DiscountCodeType;

public class DiscountCodeDetail extends DiscountCode {

    private static final long serialVersionUID = -2;
    public String detail;

    public DiscountCodeDetail(DiscountCode discount, String detail) {
        super(discount.getName(), discount.getAmount(), discount.getEnterprise(), discount.getType(), discount.getUsed());
        this.detail = detail;
    }

    public DiscountCodeDetail(String name, Integer amount, String enterprise, DiscountCodeType type, Integer used, String detail) {
        super(name, amount, enterprise, type, used);
        this.detail = detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDetail(){
        return detail;
    }

}