package org.games.discount.marshalling;

import org.games.discount.enums.DiscountCodeType;
import org.games.discount.model.DiscountCode;
import org.infinispan.protostream.GeneratedSchema;
import org.infinispan.protostream.annotations.AutoProtoSchemaBuilder;
import org.infinispan.protostream.types.java.math.BigDecimalAdapter;

@AutoProtoSchemaBuilder(
    includeClasses = { DiscountCode.class, DiscountCodeType.class, BigDecimalAdapter.class }, 
    schemaPackageName = "discount_schema")
public interface DiscountCodeSchema extends GeneratedSchema {

}