package org.games.discount.marshalling;

import java.io.IOException;

import org.games.discount.enums.DiscountCodeType;
import org.games.discount.model.DiscountCode;
import org.games.discount.services.DiscountCodes;
import org.infinispan.protostream.MessageMarshaller;
import org.infinispan.protostream.MessageMarshaller.ProtoStreamReader;
import org.infinispan.protostream.MessageMarshaller.ProtoStreamWriter;

public class DiscountCodeMarshaller implements MessageMarshaller<DiscountCode> {

    @Override
    public String getTypeName() {
        return "discount_schema.DiscountCode";
    }

    @Override
    public Class<? extends DiscountCode> getJavaClass() {
        return DiscountCode.class;
    }

    @Override
    public void writeTo(ProtoStreamWriter writer, DiscountCode code) throws IOException {
        writer.writeString("name", code.getName());
        writer.writeInt("amount", code.getAmount());
        writer.writeString("enterprise", code.getEnterprise());
        writer.writeEnum("type", code.getType());
        writer.writeInt("used", code.getUsed());
    }

    @Override
    public DiscountCode readFrom(ProtoStreamReader reader) throws IOException {
        String name = reader.readString("name");
        Integer amount = reader.readInt("amount");
        String enterprise = reader.readString("enterprise");
        DiscountCodeType type = reader.readEnum("type", DiscountCodeType.class);
        Integer used = reader.readInt("used");
        return new DiscountCode(name, amount, enterprise, type, used);
    } 

}