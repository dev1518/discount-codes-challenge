package org.games.discount.controller;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.games.discount.enums.DiscountCodeType;
import org.games.discount.model.DiscountCode;
import org.games.discount.model.DiscountCodeDetail;
import org.games.discount.services.DiscountCodes;
import org.infinispan.client.hotrod.RemoteCache;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import io.quarkus.infinispan.client.Remote;

import java.net.URI;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Path("/discounts")
public class DiscountController {

    private static final String VALUE_PATTERN = "%d euros of discount in %s used %d times";
    private static final String PERCENT_PATTERN = "%d%% discount in %s used %d times";

    @Inject
    @Remote("my-cache")
    private RemoteCache<String, DiscountCode> cache;

    /**
     * Crea el descuento
     * @param discountCode
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(DiscountCode discountCode) {
        if (!cache.containsKey(discountCode.getName())) {
            discountCode.setUsed(0);
            cache.put(discountCode.getName(), discountCode);
            return Response.created(URI.create(discountCode.getName())).build();
        }

        return Response.ok(URI.create(discountCode.getName())).build();
    }

    /**
     * Crea descuento con tiempo de expiracion
     * @param discountCode
     * @return
     */
    @POST
    @Path(value = "temporarily/{timer}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createWithExpiration(DiscountCode discountCode, @PathParam int timer) {
        if (!cache.containsKey(discountCode.getName())) {
            discountCode.setUsed(0);
            cache.put(discountCode.getName(), discountCode, timer, TimeUnit.SECONDS);
            return Response.created(URI.create(discountCode.getName())).build();
        }

        return Response.ok(URI.create(discountCode.getName())).build();
    }

    @PUT
    @Path("/consume/{name}")
    public Response consume(@PathParam("name") String name) {
        DiscountCode discountCode = cache.get(name);

        if(discountCode == null) {
            return Response.noContent().build();
        }

        discountCode.setUsed(discountCode.getUsed() + 1);
        cache.put(name, discountCode);

        return Response.ok(discountCode).build();
    }

    @GET
    @Path("/{type}")
    public DiscountCodes<DiscountCode> getByType(@PathParam("type") DiscountCodeType type) {
        List<DiscountCode> discountCodes = cache.values().stream().filter((code) -> code.getType() == type)
              .collect(Collectors.toList());
        return new DiscountCodes<DiscountCode>(discountCodes, discountCodes.size());
    }

    @GET
    @Path("{type}/details")
    public DiscountCodes<DiscountCodeDetail> getByTypeDetails(@PathParam("type") DiscountCodeType type) {
        List<DiscountCodeDetail> discountCodes = cache.values().stream().filter((code) -> code.getType() == type)
              .map((detail) -> {
                    String det= "";
                    if(DiscountCodeType.PERCENT.equals(type)){
                        det = String.format(PERCENT_PATTERN, detail.getAmount(), detail.getEnterprise(), detail.getUsed());
                    } else{
                        det = String.format(VALUE_PATTERN, detail.getAmount(), detail.getEnterprise(), detail.getUsed());
                    }
                    return new DiscountCodeDetail(detail, det);
              })
              .collect(Collectors.toList());
        return new DiscountCodes<DiscountCodeDetail>(discountCodes, discountCodes.size());
    }

}