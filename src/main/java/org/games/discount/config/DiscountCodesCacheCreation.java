package org.games.discount.config;

import io.quarkus.runtime.StartupEvent;

import org.games.discount.model.DiscountCode;
import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.client.hotrod.RemoteCacheManager;
import org.infinispan.client.hotrod.annotation.ClientCacheEntryCreated;
import org.infinispan.client.hotrod.annotation.ClientCacheEntryModified;
import org.infinispan.client.hotrod.annotation.ClientCacheEntryRemoved;
import org.infinispan.client.hotrod.annotation.ClientListener;
import org.infinispan.client.hotrod.event.ClientCacheEntryCreatedEvent;
import org.infinispan.client.hotrod.event.ClientCacheEntryModifiedEvent;
import org.infinispan.client.hotrod.event.ClientCacheEntryRemovedEvent;
import org.infinispan.commons.configuration.XMLStringConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

@ApplicationScoped
public class DiscountCodesCacheCreation {

    private static final Logger LOGGER = LoggerFactory.getLogger("DiscountsCodeCacheCreation");
    
    private static final String CACHE_CONFIG = "<distributed-cache name=\"%s\">"
          + " <encoding media-type=\"application/x-protostream\"/>"
          + "</distributed-cache>";

    @Inject
    private RemoteCacheManager remoteCacheManager;

    void onStart(@Observes StartupEvent ev) {

        RemoteCache<String, DiscountCode> cache = remoteCacheManager.administration()
            .getOrCreateCache("my-cache",
                new XMLStringConfiguration(String.format(CACHE_CONFIG, "my-cache")));

        cache.addClientListener(new EventPrintListener());

    }
    
    @ClientListener
    class EventPrintListener {
        @ClientCacheEntryCreated
        public void handleCreatedEvent(ClientCacheEntryCreatedEvent<DiscountCode> e) {
            LOGGER.info("Someone has created an entry: " + e);
        }
        @ClientCacheEntryModified
        public void handleModifiedEvent(ClientCacheEntryModifiedEvent<DiscountCode> e) {
            LOGGER.info("Someone has modified an entry: " + e);
        }
        @ClientCacheEntryRemoved
        public void handleRemovedEvent(ClientCacheEntryRemovedEvent<DiscountCode> e) {
            LOGGER.info("Someone has removed an entry: " + e);
        }
    }
}