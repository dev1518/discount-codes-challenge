package org.games.discount.services;

import io.quarkus.runtime.annotations.RegisterForReflection;

import java.util.List;

@RegisterForReflection
public class DiscountCodes <T> {
    
   private long totalCount;
   private List<T> discountCodesList;

   public DiscountCodes() {
   }

   public DiscountCodes(List<T> discountCodesList, long totalCount) {
      this.discountCodesList = discountCodesList;
      this.totalCount = totalCount;
   }

   public List<T> getDiscountCodesList() {
      return discountCodesList;
   }

   public void setDiscountCodesList(List<T> discountCodesList) {
      this.discountCodesList = discountCodesList;
   }

   public long getTotalCount() {
      return totalCount;
   }

   public void setTotalCount(long totalCount) {
      this.totalCount = totalCount;
   }
}