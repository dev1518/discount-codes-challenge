package org.games.discount.enums;

import org.infinispan.protostream.annotations.ProtoEnumValue;

public enum DiscountCodeType {
    @ProtoEnumValue(number = 1, name = "PER")
    PERCENT,
    @ProtoEnumValue(number = 2, name = "VAL")
    VALUE
}