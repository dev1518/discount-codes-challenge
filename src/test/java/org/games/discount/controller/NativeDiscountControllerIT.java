package org.games.discount.controller;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeDiscountControllerIT extends DiscountControllerTest {

    // Execute the same tests but in native mode.
}